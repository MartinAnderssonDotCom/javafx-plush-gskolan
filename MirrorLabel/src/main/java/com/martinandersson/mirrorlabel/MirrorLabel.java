package com.martinandersson.mirrorlabel;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class MirrorLabel extends Application
{
    public static void main(String... ignored) {
        Application.launch(ignored);
    }

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        
        loader.setLocation(MirrorLabel.class.getResource("MirrorLabel.fxml"));
        loader.setController(new Controller());
        VBox root = loader.load();
        
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setAlwaysOnTop(true);
        stage.show();
    }
}