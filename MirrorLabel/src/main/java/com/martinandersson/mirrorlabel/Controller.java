package com.martinandersson.mirrorlabel;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class Controller implements Initializable
{
    @FXML
    private TextField textField;
    
    @FXML
    private Label label;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        label.textProperty().bind(textField.textProperty());
    }
}
