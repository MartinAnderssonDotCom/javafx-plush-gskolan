package com.martinandersson.gui.popup;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class Popup extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }
    
    private Stage main;
    private Button button;
    
    @Override
    public void start(Stage stage) {
        main = stage;
        
        button = new Button("Click me!");
        button.setOnAction(this::onButtonClick);
        
        HBox root = new HBox();
        root.getChildren().add(button);
        root.setAlignment(Pos.CENTER);
        HBox.setMargin(button, new Insets(20, 75, 20, 75));
        
        Scene scene = new Scene(root);
        stage.setScene(scene);
        
        stage.setTitle("Clickable");
        stage.setAlwaysOnTop(true);
        stage.show();
    }
    
    private void onButtonClick(ActionEvent event) {
        HBox root = new HBox();
        TextField input = new TextField();
        
        input.setPromptText("Type something, then press enter.");
        input.setPrefColumnCount(18);
        
        root.getChildren().add(input);
        root.setAlignment(Pos.CENTER);
        HBox.setMargin(input, new Insets(20, 75, 20, 75));
        
        Scene scene = new Scene(root);
        Stage popup = new Stage();
        
        input.setOnAction(actionEvent -> {
            button.setText(input.getText());
            popup.hide();
        });
        
        popup.initOwner(main);
        popup.initModality(Modality.WINDOW_MODAL);
        popup.initStyle(StageStyle.UTILITY);
        
        popup.setScene(scene);
        popup.show();
        
        root.requestFocus();
    }
}
