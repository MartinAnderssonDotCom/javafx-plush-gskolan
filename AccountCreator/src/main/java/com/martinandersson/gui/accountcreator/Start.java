
package com.martinandersson.gui.accountcreator;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class Start extends Application
{
    public static void main(String... ignored) {
        Application.launch(ignored);
    }

    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(Start.class.getResource("FormView.fxml"));
        stage.setScene(new Scene(root));
        stage.show();
    }
}