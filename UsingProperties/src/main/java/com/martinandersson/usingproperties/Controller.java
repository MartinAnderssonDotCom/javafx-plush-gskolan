package com.martinandersson.usingproperties;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.When;
import javafx.beans.property.DoubleProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/**
 *
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class Controller implements Initializable
{
    @FXML
    private Slider slider;
    
    @FXML
    private Label half, actual, twice;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        DoubleProperty val = slider.valueProperty();
        
        half.textProperty().bind(val.divide(2).asString("%.2f"));
        actual.textProperty().bind(val.asString("%.2f"));
        
        DoubleBinding doubled = val.multiply(2);
        twice.textProperty().bind(doubled.asString("%.2f"));
        
        Paint def = twice.getTextFill();
        
        twice.textFillProperty().bind(new When(doubled.greaterThan(100))
                .then((Paint) Color.RED)
                .otherwise(def));
        
        // Test debug!
//        slider.valueProperty().addListener(observable ->
//                System.out.println("Value change to: " + slider.getValue()));
    }
}