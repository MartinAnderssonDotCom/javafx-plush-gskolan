package com.martinandersson.usingproperties;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Demonstrates how to use properties.<p>
 * 
 * See project "creating properties" for some example code demonstrating how to
 * create properties.
 * 
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class Start extends Application
{
    public static void main(String... ignored) {
        Application.launch(ignored);
    }

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader lodare = new FXMLLoader();
        
        lodare.setController(new Controller());
        lodare.setLocation(Start.class.getResource("Scene.fxml"));
        
        Parent root = lodare.load();
        
        stage.setTitle("Properties!");
        stage.setScene(new Scene(root, 400, 200));
        
        stage.show();
    }
}