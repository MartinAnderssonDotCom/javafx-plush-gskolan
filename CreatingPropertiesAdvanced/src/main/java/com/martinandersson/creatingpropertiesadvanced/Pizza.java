package com.martinandersson.creatingpropertiesadvanced;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * Demonstrates how professional code declare properties.<p>
 * 
 * <ul>
 *   <li>Read-only properties</li>
 * </ul>
 * 
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class Pizza
{
    private IntegerProperty price = new SimpleIntegerProperty(50);
    
    private ReadOnlyBooleanWrapper glutenFree = new ReadOnlyBooleanWrapper(false);
    
    // Not really safe return type here!
    public ReadOnlyIntegerProperty priceProperty() {
        return price;
    }
    
    private void setPrice(int price) {
        this.price.set(price);
    }
    
    public ReadOnlyBooleanProperty glutenFreeProperty() {
        return glutenFree.getReadOnlyProperty();
    }
    
    private void setGlutenFree(boolean glutenFree) {
        this.glutenFree.set(glutenFree);
    }
    
    public static void main(String[] args) {
        Pizza pizza = new Pizza();
        
        // Using a less safe version of a "read only" property:
        
        ReadOnlyIntegerProperty price = pizza.priceProperty();
        System.out.println("price: " + price.get());
        
        IntegerProperty writablePrice = (IntegerProperty) price;
        writablePrice.set(1);
        
        System.out.println("price: " + price.get());
        
        // Using a real read only property:
        
        ReadOnlyBooleanProperty glutenFree = pizza.glutenFreeProperty();
        
        // Both tries of mutating a read only here fail with a ClassCastException:
        
//        BooleanProperty x = (BooleanProperty) glutenFree;
//        x.set(true);
        
//        SimpleBooleanProperty y = (SimpleBooleanProperty) glutenFree;
//        y.set(true);
    }
}