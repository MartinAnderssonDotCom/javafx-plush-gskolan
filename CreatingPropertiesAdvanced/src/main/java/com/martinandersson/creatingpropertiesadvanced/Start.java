package com.martinandersson.creatingpropertiesadvanced;

/**
 * Demonstrates how professional code declare properties.<p>
 * 
 * <ul>
 *   <li>Lazy initialization - Person.java</li>
 *   <li>Read-only properties - Pizza.java</li>
 * </ul>
 * 
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class Start
{
    public static void main(String[] args) {
        // TODO: Anything?
    }
}