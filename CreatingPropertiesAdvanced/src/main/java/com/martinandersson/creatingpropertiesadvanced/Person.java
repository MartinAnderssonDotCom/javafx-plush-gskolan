package com.martinandersson.creatingpropertiesadvanced;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Demonstrates how professional code declare properties.<p>
 * 
 * <ul>
 *   <li>Lazy initialization</li>
 * </ul>
 * 
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class Person
{
    private StringProperty name;
    private String __name;
    
    public StringProperty nameProperty() {
        if (name == null) {
            name = new SimpleStringProperty(this, "name", __name);
            __name = null;
        }
        
        return name;
    }
    
    public void setName(String name) {
        if (this.name != null) {
            this.name.set(name);
        }
        else {
            __name = name;
        }
    }
    
    public String getName() {
        return this.name != null ?
                this.name.get() :
                __name;
    }
}