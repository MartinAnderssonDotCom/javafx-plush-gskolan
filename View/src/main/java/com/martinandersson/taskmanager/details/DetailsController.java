package com.martinandersson.taskmanager.details;

import com.martinandersson.taskmanager.Task;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 *
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class DetailsController implements Initializable
{
    private ObjectProperty<Task> loaded = new SimpleObjectProperty<>();
    
    @FXML
    private Label title;
    
    @FXML
    private Label description;
    
    public void load(Task model) {
        // Load model..
        
    }
    
    public void unload() {
        // Empty the view and 
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
}