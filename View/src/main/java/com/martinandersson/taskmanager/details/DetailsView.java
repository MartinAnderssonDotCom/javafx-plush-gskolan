package com.martinandersson.taskmanager.details;

import com.martinandersson.taskmanager.Task;
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class DetailsView
{
    private static final String TITLE = "Task Details";
    private static final String FXML = "Details.fxml";
    
    private final Stage stage;
    
    private final DetailsController controller;
    
    public DetailsView() throws IOException {
        stage = new Stage();
        stage.setTitle(TITLE);
        
        FXMLLoader loader = new FXMLLoader();
        
        DetailsController controller = new DetailsController();
        
        loader.setLocation(DetailsView.class.getResource(FXML));
        loader.setController("");
        
        Parent root = loader.load();
        stage.setScene(new Scene(root, 300, 150));
        
        this.controller = controller;
    }
    
    public void show() {
        stage.show();
    }
    
    public void hide() {
        stage.hide();
    }
    
    public DetailsController getController() {
        return controller;
    }
}