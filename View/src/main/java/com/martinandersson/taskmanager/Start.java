package com.martinandersson.taskmanager;

import com.martinandersson.taskmanager.details.DetailsView;
import java.io.IOException;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class Start extends Application
{
    private DetailsView view;
    
    private Button button;
    
    public static void main(String... ignored) {
        Application.launch(ignored);
    }

    @Override
    public void start(Stage stage) throws IOException {
        view = new DetailsView();
        
        StackPane root = new StackPane();
        
        button = new Button("Toggle task details..");
        button.setUserData(false); // <-- initial state of view
        
        button.setOnAction(this::toggleDetails);
        
        root.getChildren().add(button);
        
        stage.setScene(new Scene(root, 250, 250));
        stage.show();
    }
    
    private void toggleDetails(ActionEvent ignored) {
        boolean isVisible = (boolean) button.getUserData();
        
        if (isVisible) {
            view.hide();
        }
        else {
            view.show();
        }
        
        button.setUserData(!isVisible);
    }
}