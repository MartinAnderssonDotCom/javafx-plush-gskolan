package com.martinandersson.creatingproperties;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.IntegerBinding;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/**
 *
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class EditController implements Initializable
{
    private final Person model;
    
    @FXML
    private TextField name;
    
    @FXML
    private TextField age;
    
    public EditController(Person model) {
        this.model = model;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // This will cause next line to crash:
//        model.nameProperty().bind(name.textProperty());
        
        // Will crash:
//        model.setName("Kalle Karlsson");
        
        name.textProperty().addListener(observable ->
                model.setName(name.getText()));
        
        age.textProperty().addListener(observable -> {
            if (age.getText().isEmpty()) {
                age.setStyle("");
                model.ageProperty().set(0);
                return;
            }
            
            try {
                int newAge = Integer.parseInt(age.getText());
                
                if (newAge > 0) {
                    model.setAge(newAge);
                    age.setStyle("");
                }
                else {
                    age.setStyle("-fx-border-color: red;");
                }
            }
            catch (NumberFormatException e) {
                age.setStyle("-fx-border-color: red;");
            }
            
            // How can we convert a property binding of one type to another?
            
//            IntegerBinding asInt = Bindings.createIntegerBinding(() -> {
//                try {
//                    return Integer.parseInt(age.getText());
//                }
//                catch (NumberFormatException e) {
//                    return -1;
//                }
//            });
//            
//            model.ageProperty().bind(asInt);
        });
    }
}