package com.martinandersson.creatingproperties;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class Person
{
    private final StringProperty name = new SimpleStringProperty();
    
    private final IntegerProperty age = new SimpleIntegerProperty();
    
    public StringProperty nameProperty() {
        return name;
    }
    
    public IntegerProperty ageProperty() {
        return age;
    }
    
    public void setName(String name) {
        this.name.set(name);
    }
    
    public void setAge(int age) {
        this.age.set(age);
    }
    
    public String getName() {
        return name.getValueSafe(); // <-- always return a string, albeit empty if null
//        return name.get();        // <-- may return null
    }
    
    public int getAge() {
        return age.get();
    }
}