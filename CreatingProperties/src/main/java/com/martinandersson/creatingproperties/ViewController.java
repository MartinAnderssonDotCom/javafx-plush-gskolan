package com.martinandersson.creatingproperties;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.When;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 *
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class ViewController implements Initializable
{
    @FXML
    private Label name, age;
    
    private final Person model;
    
    public ViewController(Person model) {
        this.model = model;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        name.textProperty().bind(
                new When(model.nameProperty().isEmpty())
                        .then("n/a")
                        .otherwise(model.nameProperty()));
        
        model.ageProperty().addListener(observable -> {
            int modelAge = model.getAge();
            
            if (modelAge <= 0) {
                age.setText("n/a");
            }
            else {
                age.setText(String.valueOf(model.getAge()));
            }
        });
    }
}