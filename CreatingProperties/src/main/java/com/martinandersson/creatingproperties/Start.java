package com.martinandersson.creatingproperties;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Martin Andersson (webmaster at martinandersson.com)
 */
public class Start extends Application
{
    public static void main(String... ignored) {
        Application.launch(ignored);
    }

    @Override
    public void start(Stage stage) throws IOException {
        Person model = new Person();
        Parent edit = load("Edit.fxml", new EditController(model));
        Parent view = load("View.fxml", new ViewController(model));
        
        Stage editStage = newStage("Edit person", edit);
        Stage viewStage = newStage("View person", view);
        
        editStage.show();
        
        // Place view stage below edit stage:
        viewStage.setY(editStage.getY() + editStage.getHeight() + 5);
        viewStage.setX(editStage.getX());
        
        viewStage.show();
    }
    
    private Parent load(String fxml, Object controller) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        
        loader.setLocation(Start.class.getResource(fxml));
        loader.setController(controller);
        
        return loader.load();
    }
    
    private Stage newStage(String title, Parent root) {
        Stage stage = new Stage();
        stage.setTitle(title);
        stage.setScene(new Scene(root, 400, 100));
        return stage;
    }
}